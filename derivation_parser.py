import os
import re
import json


class DerivationParser:
    """
    Can be used to parse derivation names from a txt file 
    obtained from twiki (https://iconnell.web.cern.ch/iconnell/TOPQDerivations/).
    """

    def __init__(self,
                 text_filepath: str,
                 dsid_filepath='dsids.json'):
        try:
            with open(text_filepath, 'r') as f:
                self.raw_text = f.read()
        except FileNotFoundError:
            raise Exception('No .txt file found at {}'.format(text_filepath))

        try:
            with open(dsid_filepath, 'r') as f:
                self.dsids = json.load(f)
        except FileNotFoundError:
            raise Exception('No .json file found at {}'.format(dsid_filepath))

        self.result = {}

    def parse_samples(self,
                      sample_key: str,
                      ptag: str,
                      rtag: str,
                      other_tag='s3126',
                      single_rtag=True):
        """
        Parses DSIDs underv the given key.
        """
        lines = self.raw_text.split('\n')

        try:
            current_dsids = self.dsids[sample_key]
        except KeyError:
            raise Exception('{} not found in the dsid file')

        current_samples = []

        for line in lines:
            for s in line.split():
                if ptag not in s:
                    continue

                if rtag not in s:
                    continue

                if single_rtag:
                    if re.search('_r\d*.*_r\d*', line):
                        continue

                if other_tag is not None:
                    if other_tag not in s:
                        continue

                for dsid in current_dsids:
                    if str(dsid) in s:
                        current_samples.append(s)

        self.result[sample_key] = current_samples

    def save_results(self,
                     indent='\t',
                     string_delimiter='\'',
                     output_filepath='samples.txt'):
        """
        Saves obtained results to the output file.
        """
        with open(output_filepath, 'w') as out_file:
            out_file.write('Current list of samples\n\n\n')

            for key in self.result:
                out_file.write('{} samples: \n'.format(key))

                for sample in self.result[key]:
                    out_file.write('{}{}{}{},\n'.format(
                        indent, string_delimiter, sample, string_delimiter))

                out_file.write('\n')
