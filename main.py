from derivation_parser import DerivationParser


text_filepath = 'topq_derivations.txt'

ptag = 'p3629'
rtag = 'r9364'


if __name__ == '__main__':
    parser = DerivationParser(text_filepath)

    parser.parse_samples('ttbar_dil',
                         ptag=ptag,
                         rtag=rtag,
                         other_tag='s3126')

    parser.parse_samples('ttbar_dil_afii',
                         ptag=ptag,
                         rtag=rtag,
                         other_tag='a875')

    parser.parse_samples('ttbar_powher7',
                         ptag=ptag,
                         rtag=rtag,
                         other_tag='a875')

    parser.parse_samples('ttbar_mass_var',
                         ptag=ptag,
                         rtag=rtag,
                         other_tag='a875')

    parser.parse_samples('ttbar_aMCatNLO',
                         ptag=ptag,
                         rtag=rtag,
                         other_tag='a875')

    parser.parse_samples('ttbar_radhi',
                         ptag=ptag,
                         rtag=rtag,
                         other_tag='a875')

    parser.parse_samples('ttbar_nonallhad',
                         ptag=ptag,
                         rtag=rtag,
                         other_tag='s3126')

    parser.parse_samples('Wjets',
                         ptag=ptag,
                         rtag=rtag,
                         other_tag='s3126')

    parser.parse_samples('Zjets',
                         ptag=ptag,
                         rtag=rtag,
                         other_tag='s3126')

    parser.parse_samples('Diboson',
                         ptag=ptag,
                         rtag=rtag,
                         other_tag='s3126')

    parser.parse_samples('ttV',
                         ptag=ptag,
                         rtag=rtag,
                         other_tag='s3126')

    parser.parse_samples('ttH',
                         ptag=ptag,
                         rtag=rtag,
                         other_tag='s3126')

    parser.parse_samples('SingleTop',
                         ptag=ptag,
                         rtag=rtag,
                         other_tag='s3126')

    parser.parse_samples('SingleTop',
                         ptag=ptag,
                         rtag=rtag,
                         other_tag='s3126')

    parser.parse_samples('SingleTopAfii',
                         ptag=ptag,
                         rtag=rtag,
                         other_tag='a875')

    parser.parse_samples('SingleTopPh7',
                         ptag=ptag,
                         rtag=rtag,
                         other_tag='a875')

    parser.parse_samples('SingleTopAmc',
                         ptag=ptag,
                         rtag=rtag,
                         other_tag='a875')

    parser.parse_samples('SingleTopDS',
                         ptag=ptag,
                         rtag=rtag,
                         other_tag='s3126')

    parser.save_results()
