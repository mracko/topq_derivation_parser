A siple parser for TOPQ derivations.
Can be used to prepare a list of samples with 
desired DSIDs, p, r and other tags.

Lists of DSID can be set in dsids.json,
each key can be parsed using the parse_sample()
method of DerivationParser.

Resutls are stored in a .txt file whose
format can be adjusted by parameters pased 
to the save_results() method of DerivationParser.